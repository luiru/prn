# README #

Il progetto è basato su Maven e serve ad effettuare calcoli di espressioni in Notazione Polacca Inversa.

## SETUP ##

* Scaricare il progetto su Eclipse (o altro IDE, dato che è maven-based)
* Con Maven -> Install si può generare il file jar
* Si può eseguire con il comando: **java -jar rpn-1.0.jar** "3 4 – 5 +"
* Non dipende da altre librerie
* Per eseguire i test: Maven -> Test (io li ho testati da Eclipse)

## DETTAGLI IMPLEMENTATIVI / STRUTTURA DEL PROGRAMMA ##
Tramite Maven viene generato il file jar **rpn-1.0.jar**, che al suo interno specifica la classe da eseguire: **it.luiguribino.rpn.Main**

La classe Main utilizza un CalculatorConfigurator per creare un Tokenizer, per creare una serie di operatori da utilizzare e per iniettare poi le dipendenze nella classe di calcolo vera e propria, it.luiguribino.rpn.rpncalculator.calculator.Calculator.

È facile aggiungere nuovi operatori estendendo la classe base Operator. È possibile avere operatori con un numero qualsiasi di argomenti, in quanto il numero di argomenti viene specificato nel costruttore ed è gestito dinamicamente. Per questo progetto di esempio, ho pensato di inserire questi operatori, nel package it.luiguribino.rpn.rpncalculator.operators:

* **somma**: simbolo +, SumOperator
* **sottrazione**: simbolo -, SubtractionOperator
* **moltiplicazione**: simbolo *, MultiplicationOperator
* **divisione**: simbolo /, DivisionOperator
* **incremento**: simbolo ++, PlusPlusOperator. **A differenza degli altri, che sono operatori binari, questo è un operatore unario**


## ESEMPI DI ESECUZIONE DA RIGA DI COMANDO ##

   **java -jar rpn-1.0.jar**  
   Errore nel calcolo: Si prega di inserire la stringa da calcolare  

   **java -jar rpn-1.0.jar 1**  
   1 = 1.0  

   **java -jar rpn-1.0.jar "1"**  
   1 = 1.0  

   **java -jar rpn-1.0.jar "1 2 +"**  
   1 2 + = 3.0  

   **java -jar rpn-1.0.jar "1 ++ 2 +"**  
   1 ++ 2 + = 4.0  

   **java -jar rpn-1.0.jar "+"**  
   Errore nel calcolo: Stringa di calcolo malformata  

   **java -jar rpn-1.0.jar "1 2"**  
   Errore nel calcolo: Stringa di calcolo malformata  

   **java -jar rpn-1.0.jar "3 4 - 5 +"**  
   3 4 - 5 + = 4.0  

   **java -jar rpn-1.0.jar "3 4 - 5 *"**  
   3 4 - 5 * = -5.0  

   **java -jar rpn-1.0.jar "1 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 +"**  
   1 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + 1 + = 10.0  

   **java -jar rpn-1.0.jar "10 ++ ++ ++"**  
   10 ++ ++ ++ = 13.0  


## TEST ##

* è presente una batteria di test abbastanza esaustiva, che testa sia l'esecuzione corretta di calcoli di stringhe formate correttamente, sia il rilevamento di errori nel caso di stringhe malformate

* vengono testate sia le classi più interne che quelle più esterne. Ad es., i test partolo dai singoli Operator sino ad arrivare a testare anche il Main

* tutti i test vengono eseguiti senza rilevare errori, ho utilizzato il comando di eclipse: Run As -> Maven test


## ASSUNZIONI ##
Nello sviluppo di questo progetto ho assunto che:

* va bene lavorare con i double, non ci interessa dare al sistema la flessibilità di lavorare con qualsiasi tipo di Number

* per questo progetto di esempio non c'era bisogno di utilizzare un sistema di logging

* per questo progetto di esempio non era necessario usare un framework, tipo Spring. L'iniezione delle dipendenze viene effettuata dalla classe CalculatorConfigurator

* la classe Operators contiene una mappa con i simboli e gli operatori utilizzati. Avrei potuto utilizzare anche il pattern ChainOfResponsibility, in modo tale che venissero interpellati uno alla volta gli operatori, che potevano decidere se era il loro turno di effettuare il calcolo oppure no, in base al simbolo letto. Per semplicità, e per evitare l'over-engineering, ho preferito utilizzare il meccanismo della mappa nella classe Operators.

## PUNTI DI FORZA ##

* è possibile estendere facilmente il programma aggiungendo altri operatori al momento non presenti, semplicemente sottoclassando la classe Operator

* è possibile aggiungere operatori con un numero qualsiasi di argomenti. Ad es., attualmente tutti gli operatori sono binari, tranne l'operatore PlusPlus che è unario (e funzionante)

* è possibile usare i simboli di default per gli operatori oppure specificarli nel costruttore

* la classe Calcolatore non scompone direttamente la stringa in Token, ma utilizza un Tokenizer che gli viene iniettato. In questo modo, può cambiare il modo in cui viene tokenizzata la stringa senza modificare il calcolatore.

## POSSIBILI PUNTI DI MIGLIORAMENTO ##

* aggiungere altri operatori

* creare una gerarchia delle eccezioni (per questo progetto di esempio ho creato una sola classe, StringaMalformataException

* esiste già un tokenizer che prende in input una stringa e restituisce un array di Token. Con poco sforzo si può fare in modo che venga accettata dal calcolare l'espressione sotto forma di lista, di array, ecc. invece che sotto forma di stringa