package it.luiguribino.rpn;

import static org.junit.Assert.*;

import org.junit.Test;

public class MainTester {
	
	@Test(expected=IllegalArgumentException.class)
	public void testNull() {
		(new Main()).calcola(null);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyList() {
		(new Main()).calcola(new String[]{});
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyString() {
		(new Main()).calcola(new String[]{""});
	}
	
	@Test
	public void test1Number() {
		Double res = (new Main()).calcola(new String[]{"1"});
		assertEquals(res, (Double)1.0);
	}
	
	@Test
	public void test1Op() {
		Double res = (new Main()).calcola(new String[]{"1 2 +"});
		assertEquals(res, (Double)3.0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testMalformed1() {
		Double res = (new Main()).calcola(new String[]{"1 2 + +"});
		assertEquals(res, (Double)3.0);
	}
	
}
