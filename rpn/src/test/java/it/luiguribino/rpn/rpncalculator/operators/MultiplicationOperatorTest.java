package it.luiguribino.rpn.rpncalculator.operators;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MultiplicationOperatorTest {
	
	private MultiplicationOperator operator= new MultiplicationOperator();
	
	@Test
	public void testMultiplication1() {
		Double result = operator.executeOperator(new Double[]{6.0, 3.0});
		assertEquals("6 x 3 = 18", new Double(18.0), result);
	}

	@Test
	public void testMultiplication2() {
		Double result = operator.executeOperator(new Double[]{3.0, 6.0});
		assertEquals("3 x 6 = 18", new Double(18), result);
	}

	@Test
	public void testMultiplicationByZero() {
		Double result = operator.executeOperator(new Double[]{3.0, 0.0});
		assertEquals("moltiplico un numero per zero = 0", (Double) 0.0, result);
	}

	@Test
	public void testMultiplicationZeroByZero() {
		Double result = operator.executeOperator(new Double[]{0.0, 0.0});
		assertEquals("moltiplico 0 x 0 = 0", (Double) 0.0, result);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionNullArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionZeroArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionOneArgument() {
		Double result = operator.executeOperator(new Double[]{3.0});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionThreeArguments() {
		Double result = operator.executeOperator(new Double[]{3.0, 4.0, 5.0});
	}

}
