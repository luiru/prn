package it.luiguribino.rpn.rpncalculator.calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorTest {

	CalculatorConfigurator cc = new CalculatorConfigurator();
	Calculator calc = cc.getCalculator();

	@Test(expected = IllegalArgumentException.class)
	public void testNull() {
		calc.calculate(null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmpty() {
		calc.calculate(" ");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testEmpty2() {
		calc.calculate("  ");
	}

	@Test
	public void testOneNumberOk() {
		calc.calculate("1");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testTwoNumbers() {
		double result = calc.calculate("1 2");
		System.out.println(result);
	}

	@Test
	public void testDivision() {
		Double result = calc.calculate("6 3 /");
		assertEquals("6/3=2", (Double) 2.0, result);
	}

	@Test
	public void testWikipediaString() {
		Double result = calc.calculate("5 1 2 + 4 * + 3 -");
		assertEquals("5 1 2 + 4 * + 3 − = 14", (Double) 14.0, result);
	}

	@Test
	public void test1() {
		Double result = calc.calculate("4 2  5 * + 1 3 2 * + /");
		assertEquals("4 2 5 * + 1 3 2 * + / = 2", (Double) 2.0, result);
	}

	@Test
	public void testMultipleSpaces() {
		Double result = calc.calculate("4 2 5  * + 1 3 2 * + /");
		assertEquals("4 2  5 * + 1 3 2 * + / = 2", (Double) 2.0, result);
	}

	@Test
	public void testMultipleSpaces2() {
		String s = " 4 2 5 * + 1 3 2 * + / ";
		Double result = calc.calculate(s);
		assertEquals(s + "= 2", (Double) 2.0, result);
	}

	@Test
	public void testMultipleSpaces3() {
		String s = "  4   2 5 * + 1 3 2 * + / ";
		Double result = calc.calculate(s);
		assertEquals(s + "= 2", (Double) 2.0, result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOperatoreIniziale() {
		String s = "+";
		Double result = calc.calculate(s);
		assertEquals(s + "= 2", (Double) 2.0, result);
	}

	@Test
	public void test6() {
		String s = "2 4 / 5 6 - *";
		Double result = calc.calculate(s);
		assertEquals(s + "= -0.5", (Double) (- 0.5), result);
	}

	@Test
	public void test7() {
		String s = "3 5 6 + *";
		Double result = calc.calculate(s);
		assertEquals(s + "= 33", (Double) 33.0, result);
	}

}
