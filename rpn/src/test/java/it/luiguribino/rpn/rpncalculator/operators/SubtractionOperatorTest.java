package it.luiguribino.rpn.rpncalculator.operators;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SubtractionOperatorTest {
	
	private SubtractionOperator operator = new SubtractionOperator();
	
	@Test
	public void testSubtraction1() {
		Double result = operator.executeOperator(new Double[]{6.0, 3.0});
		assertEquals("6 - 3 = 3", new Double(3.0), result);
	}

	@Test
	public void testSubtraction2() {
		Double result = operator.executeOperator(new Double[]{3.0, 6.0});
		assertEquals("3 - 6 = -3", new Double(-3.0), result);
	}

	@Test
	public void testSubtractionMinusZero() {
		Double result = operator.executeOperator(new Double[]{3.0, 0.0});
		assertEquals("tolgo zero ad un numero: 3 - 0 = 3", (Double) 3.0, result);
	}

	@Test
	public void testSubtractionZeroPlusZero() {
		Double result = operator.executeOperator(new Double[]{0.0, 0.0});
		assertEquals("0 - 0 = 0", (Double) 0.0, result);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionNullArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionZeroArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionOneArgument() {
		Double result = operator.executeOperator(new Double[]{3.0});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionThreeArguments() {
		Double result = operator.executeOperator(new Double[]{3.0, 4.0, 5.0});
	}

}
