package it.luiguribino.rpn.rpncalculator.calculator;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import it.luiguribino.rpn.rpncalculator.operators.DivisionOperator;
import it.luiguribino.rpn.rpncalculator.operators.MultiplicationOperator;
import it.luiguribino.rpn.rpncalculator.operators.Operators;
import it.luiguribino.rpn.rpncalculator.operators.PlusPlusOperator;
import it.luiguribino.rpn.rpncalculator.operators.SubtractionOperator;
import it.luiguribino.rpn.rpncalculator.operators.SumOperator;

public class CalculatorConfiguratorTest {
	
	
	@Test
	public void testPresenzaDiTutteLeOperazioni() {
		CalculatorConfigurator ct = new CalculatorConfigurator();
		Calculator calc = ct.getCalculator();
		Operators operators = calc.getOperators();
		assertNotNull(operators.getOperatorBySymbol(DivisionOperator.DEFAULT_SYMBOL));
		assertNotNull(operators.getOperatorBySymbol(MultiplicationOperator.DEFAULT_SYMBOL));
		assertNotNull(operators.getOperatorBySymbol(SumOperator.DEFAULT_SYMBOL));
		assertNotNull(operators.getOperatorBySymbol(PlusPlusOperator.DEFAULT_SYMBOL));
		assertNotNull(operators.getOperatorBySymbol(SubtractionOperator.DEFAULT_SYMBOL));
	}
}
