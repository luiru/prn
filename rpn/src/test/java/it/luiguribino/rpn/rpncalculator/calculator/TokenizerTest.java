package it.luiguribino.rpn.rpncalculator.calculator;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class TokenizerTest {
	
	@Test
	public void testTrim() {
		Tokenizer tk = new Tokenizer();
		String[] tokens = tk.getTokens("  2 3 +    ");
		assertEquals(Arrays.asList(tokens), Arrays.asList(new String[] {"2", "3", "+"}));
	}
	
	@Test
	public void testSpaziMultipli() {
		Tokenizer tk = new Tokenizer();
		String[] tokens = tk.getTokens("  2  3             +    ");
		assertEquals(Arrays.asList(tokens), Arrays.asList(new String[] {"2", "3", "+"}));
	}
	
}
