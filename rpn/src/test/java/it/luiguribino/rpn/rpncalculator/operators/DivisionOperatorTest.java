package it.luiguribino.rpn.rpncalculator.operators;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DivisionOperatorTest {
	
	private DivisionOperator operator = new DivisionOperator("+");
	
	@Test
	public void testDivision1() {
		Double result = operator.executeOperator(new Double[]{6.0, 3.0});
		assertEquals("divideno maggiore del divisore", new Double(2.0), result);
	}

	@Test
	public void testDivision2() {
		Double result = operator.executeOperator(new Double[]{3.0, 6.0});
		assertEquals("dividendo minore del divisore", new Double(0.5), result);
	}

	@Test
	public void testDivisionByZero() {
		Double result = operator.executeOperator(new Double[]{3.0, 0.0});
		assertEquals("dividendo minore del divisore", (Double) Double.POSITIVE_INFINITY, result);
	}

	@Test
	public void testDivisionZeroByZero() {
		Double result = operator.executeOperator(new Double[]{0.0, 0.0});
		assertEquals("dividendo minore del divisore", (Double) Double.NaN, result);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionNullArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionZeroArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionOneArgument() {
		Double result = operator.executeOperator(new Double[]{3.0});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testDivisionThreeArguments() {
		Double result = operator.executeOperator(new Double[]{3.0, 4.0, 5.0});
	}

}
