package it.luiguribino.rpn.rpncalculator.operators;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlusPlusOperatorTest {
	
	private PlusPlusOperator operator = new PlusPlusOperator();
	
	@Test
	public void testPlusPlus1() {
		Double result = operator.executeOperator(new Double[]{6.0});
		assertEquals("6++ = 7", new Double(7.0), result);
	}

	@Test
	public void testPlusPlus2() {
		Double result = operator.executeOperator(new Double[]{-3.0});
		assertEquals("-3++ = -2", new Double(-2.0), result);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOperatorNullArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOperatorZeroArguments() {
		Double result = operator.executeOperator(new Double[]{});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOperatorTworguments() {
		Double result = operator.executeOperator(new Double[]{3.0, 4.0});
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOperatorThreeArguments() {
		Double result = operator.executeOperator(new Double[]{3.0, 4.0, 5.0});
	}

}
