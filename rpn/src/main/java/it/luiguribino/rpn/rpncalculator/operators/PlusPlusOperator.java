package it.luiguribino.rpn.rpncalculator.operators;


public class PlusPlusOperator extends Operator {

	public static final String DEFAULT_SYMBOL = "++";

	public PlusPlusOperator() {
		this(DEFAULT_SYMBOL);
	}

	public PlusPlusOperator(String operatorSymbol) {
		super(Operator.UNARY_OPERATOR_OPERANDS, operatorSymbol);
	}

	@Override
	protected double executeOperatorTemplate(Double... numbers) {
		double result = numbers[0] + 1;
		// System.out.println(numbers[0] + " ++ " + " = " + result);
		return result;
	}

}
