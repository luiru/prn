package it.luiguribino.rpn.rpncalculator.operators;

/**
 *
 * @author luigi
 */
public class SubtractionOperator extends Operator {

	public static final String DEFAULT_SYMBOL = "-";

	public SubtractionOperator() {
		this(DEFAULT_SYMBOL);
	}

	public SubtractionOperator(String operatorSymbol) {
		super(Operator.BINARY_OPERATOR_OPERANDS, operatorSymbol);
	}

	@Override
	protected double executeOperatorTemplate(Double... numbers) {
		double result = numbers[0] - numbers[1];
		// System.out.println(numbers[0] + " - " + numbers[1] + " = " + result);
		return result;
	}

}
