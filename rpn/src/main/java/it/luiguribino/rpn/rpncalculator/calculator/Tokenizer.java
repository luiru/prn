package it.luiguribino.rpn.rpncalculator.calculator;

/**
 *
 * @author luigi
 */
public class Tokenizer {
    
    public Tokenizer() {
    }
    
    /** Tokenizza la stringa in input. Il metodo permette di usare uno o più spazi come separatori, come pure permette di inserire spazi iniziali e finali.
     * Viene utilizzata una espressione regolare per rimuovere gli spazi aggiuntivi.
     * @param calculation
     * @return
     */
    public String[] getTokens(String calculation) {
    	if (null == calculation) {
    		throw new StringaMalformataException(StringaMalformataException.EMPTY_ERROR_MESSAGE);
    	}
    	calculation = calculation.trim().replaceAll("\\s+"," ");
        return calculation.split(" ");
    } 
    
}
