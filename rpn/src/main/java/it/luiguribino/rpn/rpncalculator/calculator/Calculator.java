package it.luiguribino.rpn.rpncalculator.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

import it.luiguribino.rpn.rpncalculator.operators.Operator;
import it.luiguribino.rpn.rpncalculator.operators.Operators;

public class Calculator {

	private Operators operators;
	private Tokenizer tokenizer;

	public Calculator(Tokenizer tokenizer, Operators operators) {
		this.tokenizer = tokenizer;
		this.operators = operators;
	}

	public double calculate(String calculationString) {
		Deque<Double> deque = new ArrayDeque<>();
		String[] tokens = tokenizer.getTokens(calculationString);
		if (tokens.length == 0) {
			throw new StringaMalformataException(StringaMalformataException.EMPTY_ERROR_MESSAGE);
		}
		for (String token : tokens) {
			Double n = getNumber(token);
			if (n != null) {
				deque.push(n);
			} else {
				// non è un numero, deve essere un operatore
				Operator operator = getOperator(token);
				Double[] operands = new Double[operator.getNumberOfOperands()];
				if (deque.size() < operator.getNumberOfOperands()) {
					throw new StringaMalformataException();
				}
				for (int i = operator.getNumberOfOperands() - 1; i >= 0; i--) {
					operands[i] = deque.pop();
				}
				double result = operator.executeOperator(operands);
				deque.push(result);
			}
		}
		try {
			if (deque.size() != 1) {
				throw new StringaMalformataException();
			}
			Double result = deque.pop();
			return result;
		} catch (Exception ex) {
			throw new StringaMalformataException();
		}
	}

	public Double getNumber(String token) {
		Double n = null;
		try {
			n = new Double(token);
		} catch (Exception ex) {
		}
		return n;
	}

	public Operator getOperator(String token) {
		Operator operator = operators.getOperatorBySymbol(token);
		if (operator == null) {
			throw new StringaMalformataException(StringaMalformataException.MISSING_OPERATOR_ERROR_MESSAGE + token);
		}
		return operator;
	}

	public Operators getOperators() {
		return operators;
	}

	public Tokenizer getTokenizer() {
		return tokenizer;
	}

}
