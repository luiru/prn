package it.luiguribino.rpn.rpncalculator.operators;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Operators {
	
	private Map<String, Operator> symbolToOperatorMap = new HashMap<>();
	
	public Operators(List<Operator> operators) {
		for (Operator op:operators) {
			addOperator(op);
		}
	}
	
	private void addOperator(Operator op) {
		String opSymbol = op.getOperatorSymbol();
		if (symbolToOperatorMap.containsKey(opSymbol)) {
			throw new IllegalStateException("Si sta inserendo un altro operatore con lo stesso simbolo di uno già inserito: " + opSymbol);
		}
		symbolToOperatorMap.put(opSymbol, op);
	}
	
	public Operator getOperatorBySymbol(String symbol) {
		Operator op = symbolToOperatorMap.get(symbol);
		return op;
	}
	
}
