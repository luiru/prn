package it.luiguribino.rpn.rpncalculator.operators;

/**
 *
 * @author luigi
 */
public class DivisionOperator extends Operator {
    
	public static final String DEFAULT_SYMBOL = "/";
	
    public DivisionOperator() {
		this(DEFAULT_SYMBOL);
    }
    
    public DivisionOperator(String operatorSymbol) {
        super(Operator.BINARY_OPERATOR_OPERANDS, operatorSymbol);
    }
    
    @Override
    protected double executeOperatorTemplate(Double... numbers) {
    	double result = numbers[0] / numbers[1];
    	// System.out.println(numbers[0] + " / " + numbers[1] + " = " + result);
    	return result;
    }
    
}
