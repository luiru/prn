package it.luiguribino.rpn.rpncalculator.operators;

import it.luiguribino.rpn.rpncalculator.calculator.StringaMalformataException;

/**
 *
 * @author luigi
 */
public abstract class Operator {
	
	public static final int UNARY_OPERATOR_OPERANDS = 1;
	public static final int BINARY_OPERATOR_OPERANDS = 2;
    
    private int numberOfOperands = 2;
    
    private String operatorSymbol = null;
    
    public Operator(int numberOfOperands, String operatorSymbol) {
        if (numberOfOperands < 0) {
            throw new StringaMalformataException("Il numero di operandi deve essere maggiore di 0");
        }
        if (operatorSymbol == null || "".equals(operatorSymbol)) {
            throw new StringaMalformataException("Il simbolo non può essere null o vuoto");
        }
        this.numberOfOperands = numberOfOperands;
        this.operatorSymbol = operatorSymbol;
    }
    
    public double executeOperator(Double ... numbers) {
        if (numbers == null) {
            throw new StringaMalformataException("la lista non può essere null");
        }
        if (numbers.length != numberOfOperands) {
            throw new StringaMalformataException("numero errato di operandi: expected=" + numberOfOperands + ", found=" + numbers.length);
        }
        return executeOperatorTemplate(numbers);
    }

    protected abstract double executeOperatorTemplate(Double ... numbers);
    
    public int getNumberOfOperands() {
        return numberOfOperands;
    }

    public void setNumberOfOperands(int numberOfOperands) {
        this.numberOfOperands = numberOfOperands;
    }

    public String getOperatorSymbol() {
        return operatorSymbol;
    }

    public void setOperatorSymbol(String operatorSymbol) {
        this.operatorSymbol = operatorSymbol;
    }
    
}
