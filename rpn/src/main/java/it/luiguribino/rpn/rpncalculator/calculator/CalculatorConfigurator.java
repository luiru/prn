package it.luiguribino.rpn.rpncalculator.calculator;

import java.util.ArrayList;
import java.util.List;

import it.luiguribino.rpn.rpncalculator.operators.DivisionOperator;
import it.luiguribino.rpn.rpncalculator.operators.MultiplicationOperator;
import it.luiguribino.rpn.rpncalculator.operators.Operator;
import it.luiguribino.rpn.rpncalculator.operators.Operators;
import it.luiguribino.rpn.rpncalculator.operators.PlusPlusOperator;
import it.luiguribino.rpn.rpncalculator.operators.SubtractionOperator;
import it.luiguribino.rpn.rpncalculator.operators.SumOperator;

public class CalculatorConfigurator {

	public Calculator getCalculator() {
		Tokenizer tokenizer = new Tokenizer();
		List<Operator> ops = new ArrayList<>();
		ops.add(new DivisionOperator("/"));
		ops.add(new MultiplicationOperator("*"));
		ops.add(new SumOperator("+"));
		ops.add(new SubtractionOperator("-"));
		ops.add(new PlusPlusOperator("++"));
		Operators operators = new Operators(ops);
		Calculator calc = new Calculator(tokenizer, operators);
		return calc;
	}

}
