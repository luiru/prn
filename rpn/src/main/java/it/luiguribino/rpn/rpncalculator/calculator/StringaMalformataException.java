package it.luiguribino.rpn.rpncalculator.calculator;

public class StringaMalformataException extends IllegalArgumentException {
	
	private static final long serialVersionUID = 1L;
	public static final String DEFAULT_ERROR_MESSAGE = "Stringa di calcolo malformata";
	public static final String EMPTY_ERROR_MESSAGE = "Stringa di calcolo vuota";
	public static final String MISSING_OPERATOR_ERROR_MESSAGE = "Operatore non presente: ";
	
	public StringaMalformataException(String message) {
		super(message);
	}
	
	public StringaMalformataException() {
		super(DEFAULT_ERROR_MESSAGE);
	}
	
}
