package it.luiguribino.rpn;

import it.luiguribino.rpn.rpncalculator.calculator.Calculator;
import it.luiguribino.rpn.rpncalculator.calculator.CalculatorConfigurator;

/**
 *
 * @author luigi
 */
public class Main {
    
    public static void main(String[] args) {
    	Main main = new Main();
    	main.doMain(args);
    }
    
    public void doMain(String[] args) {
    	try {
    		double result = calcola(args);
        	String calcString = args[0];
            System.out.println(calcString + " = " + result);

    	} catch (Exception ex) {
    		System.err.println("Errore nel calcolo: " + ex.getMessage());
    	}
    }
    
    public double calcola(String[] args) {
        if (args == null || args.length == 0) {
        	throw new IllegalArgumentException("Si prega di inserire la stringa da calcolare");
        } else {
        	String calcString = args[0];
            CalculatorConfigurator cc = new CalculatorConfigurator();
            Calculator calc = cc.getCalculator();
            double result = calc.calculate(calcString);
            return result;
        }
    }
}


/*

Supposizioni:
1. va bene lavorare con i double, non ci interessa dare al sistema la flessibilità di lavorare con qualsiasi tipo di Number
2. per questo progetto di esempio non c'era bisogno di utilizzare un sistema di logging
3. per questo progetto di esempio non era necessario usare un framework, tipo Spring. L'iniezione delle dipendenze viene effettuata dalla classe CalculatorConfigurator
4. la classe Operators contiene una mappa con i simboli e gli operatori utilizzati. Avrei potuto utilizzare anche il pattern ChainOfResponsibility, in modo tale che venissero interpellati uno alla volta gli operatori, che potevano decidere se era il loro turno di effettuare il calcolo oppure no, in base al simbolo letto. Per semplicità, e per evitare l'over-engineering, ho preferito utilizzare il meccanismo della mappa.

Punti di forza:
1. è possibile estendere facilmente il programma aggiungendo altri operatori al momento non presenti, semplicemente sottoclassando la classe Operator
2. è possibile aggiungere operatori con un numero qualsiasi di argomenti. Ad es., attualmente tutti gli operatori sono binari, tranne l'operatore PlusPlus che è unario (e funzionante)
3. è possibile usare i simboli di default per gli operatori oppure specificarli nel costruttore
4. la classe Calcolatore non scompone direttamente la stringa in Token, ma utilizza un Tokenizer che gli viene iniettato. In questo modo, può cambiare il modo in cui viene tokenizzata la stringa senza modificare il calcolatore.

Punti di miglioramento:
1. creare una gerarchia delle eccezioni
2. esiste già un tokenizer, con poco sforzo si può fare in modo che venga accettata dal calcolare l'espressione sotto forma di lista, di array, ecc. invece che sotto forma di stringa


*/